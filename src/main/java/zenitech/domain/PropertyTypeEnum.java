package zenitech.domain;

public enum PropertyTypeEnum {
    APARTMENT, HOUSE, INDUSTRIAL, OTHER
}
