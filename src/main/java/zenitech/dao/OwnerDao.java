package zenitech.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zenitech.domain.Owner;

@Repository
public interface OwnerDao extends JpaRepository<Owner, Long> {
    Owner findByPersonalCode(String personalCode);
}
