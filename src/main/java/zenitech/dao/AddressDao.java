package zenitech.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zenitech.domain.Address;

public interface AddressDao extends JpaRepository<Address, Long> {
}
