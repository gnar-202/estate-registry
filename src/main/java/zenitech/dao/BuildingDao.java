package zenitech.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zenitech.domain.Address;
import zenitech.domain.Building;
import zenitech.domain.Owner;

import java.util.List;

@Repository
public interface BuildingDao extends JpaRepository<Building, Long> {
    List<Building> findAllByOwner(Owner owner);
    List<Building> findBuildingsByAddress(Address address);
}
