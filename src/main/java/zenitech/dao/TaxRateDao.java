package zenitech.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zenitech.domain.PropertyTypeEnum;
import zenitech.domain.TaxRate;

public interface TaxRateDao extends JpaRepository<TaxRate, Long> {
    TaxRate findTaxRateByPropertyType(PropertyTypeEnum propertyTypeEnum);
}
